<?php

		//1 PHP String Cast Failure to Built-in Function Bug
		$test = ["init"];
        var_dump(array_search(0, $test));               //Prints int(0)
        var_dump(array_search("init", $test));          //Prints int(0)
        var_dump(array_search(0, $test, true));         //Prints bool(false)
        var_dump(array_search("init", $test, true));    //Prints int(0)

        /*	
		Potential explanation:
			When calling array_search(), PHP internally calls the fast_equal_check_long() function because the value looked for is 0, with an internal LONG type
			Then, fast_equal_check_long() calls zend_compare() because "init" is not of type LONG as 0 is
			
			Here's the trick:When comparing 0 and "init" values, in this particular unhandled case when value (int 0) is LONG and entry (string "init") is STRING, PHP calls the _zendi_convert_scalar_to_number_silent() as it's default's
			BUT
			In _zendi_convert_scalar_to_number_silent(), the value 0 is comparable to FALSE
			Then, it triggers the IS_FALSE case when checking the Z_TYPE_P of 0, and converts "init" to it's LONG/INT value, which is 0.
			
			Why is a string value is 0 ?
			
			ZVAL_LONG(), the function used in conversio process, is used as
			
			ZVAL_LONG(holder, 0);
			
			Where holder is the string passed "init", and... which basically sets the value of each character to 0, hardcoded
			
			Then, the converted "init" value to 0 passes the check with zend_compare(), then with fast_equal_check_long(), finally with array_search().
			
			Sources:
			array_search(): https://github.com/php/php-src/blob/bcd7352b0c06b7fd41622789e6821bbabb968a8e/ext/standard/array.c
			fast_equal_check_long() used by array_search(): https://github.com/php/php-src/blob/c0997130ec30f36ed1f87929b676f2521c84b2e2/Zend/zend_operators.h
			zend_compare() used by fast_equal_check_long(): https://github.com/php/php-src/blob/92c4b0651368a4135f692af48f4716c96de4bd92/Zend/zend_operators.c
			_zendi_convert_scalar_to_number_silent() used by zend_compare(): https://github.com/php/php-src/blob/92c4b0651368a4135f692af48f4716c96de4bd92/Zend/zend_operators.c
			ZVAL_LONG() for string conversion: https://github.com/php/php-src/blob/15846ff115722b2f95d699abf07141d774b0e2cd/Zend/zend_types.h
        */
        var_dump((int)"whateverstring"); //Prints int(0)
		die; //This is me rn
		
		
		//2 Conflictual Identifiers for DateInternal
		/*
			When using a Datetime class, string identifiers for both month and minutes are identical, making them impossible to identifiate clearly
			@See /assets/dateinterval_perioddesignator.png
			A workaround is to follow ISO8601 standard and prepend a `T` string for identify time (thanks owen[at]beliefs.com for contribution)
		*/
		
		$interval1 = new DateInterval("P1M"); //Can identify both one month or minute for dev. Who knows ?
		$interval2 = new DateInterval("PT1M"); // Workaround to identify minute
		
		var_dump($interval1); //Interval set to one month
		var_dump($interval2); //Interval set to one minute
		die; //Me rn